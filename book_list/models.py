from unicodedata import name
from django.db import models


class Books(models.Model):
    name = models.CharField(max_length=100, unique=True)
    author = models.CharField(max_length=50)
    page_count = models.SmallIntegerField(max_length=20)
    isbn = models.SmallIntegerField(max_length=20)
    year_published = models.SmallIntegerField(null=True)
    cover = models.URLField(null=True, blank=True)
    in_print= models.BooleanField()
    description = models.CharField(max_length=200, null=True)

    def __string__(self):
        return self.name + " by " + self.author
